terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 3.12.0"
    }
    scaleway = {
      source  = "scaleway/scaleway"
      version = "~> 2.19.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.3.2"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.13.0"
    }
    grafana = {
      source  = "grafana/grafana"
      version = "~> 1.27.0"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "~> 4.0.4"
    }
  }
  required_version = "~> 1.7"
}
