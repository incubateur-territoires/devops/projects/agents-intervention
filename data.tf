data "scaleway_k8s_cluster" "prod" {
  provider   = scaleway.scaleway_production
  cluster_id = var.scaleway_cluster_production_cluster_id
}

data "scaleway_k8s_cluster" "dev" {
  provider   = scaleway.scaleway_development
  cluster_id = var.scaleway_cluster_development_cluster_id
}

data "gitlab_project" "mobile" {
  id = 42912728
}
data "gitlab_project" "api" {
  id = 42817168
}
data "gitlab_project" "bureau" {
  id = 44662856
}
data "gitlab_project" "landing" {
  id = 46063789
}

locals {
  gitlab_project_ids = {
    api     = data.gitlab_project.api.id
    mobile  = data.gitlab_project.mobile.id
    bureau  = data.gitlab_project.bureau.id
    landing = data.gitlab_project.landing.id
  }
}
