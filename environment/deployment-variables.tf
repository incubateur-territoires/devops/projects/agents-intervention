module "configure_landing_for_deployment" {
  source  = "gitlab.com/vigigloo/tf-modules/gitlabk8svariables"
  version = "0.1.0"

  repositories = [var.gitlab_project_ids.landing]
  scope        = var.gitlab_environment_scope

  cluster_user           = module.namespace.user
  cluster_token          = module.namespace.token
  cluster_namespace      = module.namespace.namespace
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = base64decode(var.kubeconfig.cluster_ca_certificate)

  base-domain = var.gitlab_environment_scope == "production" ? var.base_domain : "landing.${var.base_domain}"
}

resource "gitlab_project_variable" "helm_values_landing" {
  environment_scope = var.gitlab_environment_scope
  project           = var.gitlab_project_ids.landing
  variable_type     = "file"

  key   = "HELM_UPGRADE_VALUES"
  value = <<-EOT
  ingress:
    enabled: false
    annotations:
      kubernetes.io/ingress.class: haproxy
  resources:
    limits:
      memory: 512Mi
    requests:
      cpu: 100m
  probes:
    liveness:
      path: /
      initialDelaySeconds: 0
    readiness:
      path: /
      initialDelaySeconds: 0
  service:
    targetPort: 3000
  serviceAccount:
    create: false
  podAnnotations:
    monitoring-org-id: ${var.monitoring_org_id}
EOT
}
