variable "base_domain" {
  type = string
}

variable "project_slug" {
  type = string
}

variable "project_name" {
  type = string
}

variable "gitlab_environment_scope" {
  type = string
}
locals {
  environment_name = var.gitlab_environment_scope == "*" ? "reviews" : var.gitlab_environment_scope
}

variable "namespace" {
  type = string
}

variable "namespace_quota_max_cpu_requests" {
  type    = string
  default = "2"
}

variable "namespace_quota_max_memory_limits" {
  type    = string
  default = "12Gi"
}

variable "monitoring_org_id" {
  type = string
}

variable "gitlab_project_ids" {
  type = object({
    api     = number
    mobile  = number
    bureau  = number
    landing = number
  })
}
