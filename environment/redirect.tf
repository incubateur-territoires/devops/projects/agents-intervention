locals {
  old_domain = "agentsenintervention.fr"
  new_domain = "agentsenintervention.anct.gouv.fr"
}
module "redirection" {
  count   = var.gitlab_environment_scope == "production" ? 1 : 0
  source  = "gitlab.com/vigigloo/tools-k8s/nginxredirect"
  version = "0.1.0"

  chart_name    = "redirection"
  chart_version = "0.1.0"
  namespace     = module.namespace.namespace

  values = [
    <<-EOT
    ingress:
      className: null
      enabled: true
      certManagerClusterIssuer: letsencrypt-prod
      host: ${local.old_domain}
      annotations:
        acme.cert-manager.io/http01-edit-in-place: "true"
        cert-manager.io/cluster-issuer: letsencrypt-prod
        kubernetes.io/ingress.class: haproxy
    EOT
  ]
  redirect_from = local.old_domain
  redirect_to   = "https://${local.new_domain}"

  requests_cpu    = "10m"
  requests_memory = "20Mi"
}
