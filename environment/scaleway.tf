resource "scaleway_object_bucket" "uploads" {
  name   = "${var.project_slug}-${local.environment_name}-uploads"
  region = "fr-par"
}
