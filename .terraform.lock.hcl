# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/gitlabhq/gitlab" {
  version     = "3.12.0"
  constraints = "~> 3.12.0"
  hashes = [
    "h1:zZKpUsf1STvLLyH30a6ZcjQoARU8MJGbHsGCjYHrnOA=",
    "zh:1c3e89cf19118fc07d7b04257251fc9897e722c16e0a0df7b07fcd261f8c12e7",
    "zh:207f7ffaaf16f3d0db9eec847867871f4a9eb9bb9109d9ab9c9baffade1a4688",
    "zh:2360bdd3253a15fbbb6581759aa9499235de670d13f02272cbe408cb425dc625",
    "zh:2a9bec466163baeb70474fab7b43aababdf28b240f2f4917299dfe1a952fd983",
    "zh:3a6ea735b5324c17aa6776bff3b2c1c2aeb2b6c20b9c944075c8244367eb7e4c",
    "zh:3aa73c622187c06c417e1cd98cd83f9a2982c478e4f9f6cd76fbfaec3f6b36e8",
    "zh:51ace107c8ba3f2bb7f7d246db6a0428ef94aafceca38df5908167f336963ec8",
    "zh:53a35a827596178c2a12cf33d1f0d0b4cf38cd32d2cdfe2443bdd6ebb06561be",
    "zh:5bece68a724bffd2e66293429319601d81ac3186bf93337c797511672f80efd0",
    "zh:60f21e8737be59933a923c085dcb7674fcd44d8a5f2e4738edc70ae726666204",
    "zh:9fb277f13dd8f81dee7f93230e7d1593aca49c4788360c3f2d4e42b8f6bb1a8f",
    "zh:ac63a9b4a3a50a3164ee26f1e64cc6878fcdb414f50106abe3dbeb7532edc8cd",
    "zh:ed083de9fce2753e3dfeaa1356508ecb0f218fbace8a8ef7b56f3f324fff593b",
    "zh:ed966026c76a334b6bd6d621c06c5db4a6409468e4dd99d9ad099d3a9de22768",
  ]
}

provider "registry.opentofu.org/grafana/grafana" {
  version     = "1.27.0"
  constraints = "~> 1.27.0"
  hashes = [
    "h1:ifKgRrZHspeXMSavSCsWbLrk6tC1NOyWN0iiGv2j6I4=",
    "zh:01ef0ae20530a54cbb4bffc35e97733916e5ae2e8f7fa00aefa2e86e24206823",
    "zh:08a4ac8b690bab9a3b454c3d998917f4ed49fc225a21ff53ceb0488eb4b9d15d",
    "zh:0e08516cd6c2495bc83a4a8e0252bfa70e310aa400af0fe766bbe7ddd05a21cb",
    "zh:14856865f6e6695e6d7708d70844a2c031cfc9b091e7cf530a453b2f78c9a691",
    "zh:2b1c05fff5011ab83acdd292484857fe886cd113abbb7fc617bbb8f358517cc0",
    "zh:31bae1b1c635a94329470b30986d336f4b3819bf24aacd953d5b57debb83bd4d",
    "zh:352b6ea190711c8f3f107540c8943c8f6b9faf4fbc73a9c1721b15db4a103edb",
    "zh:7eda29d30d451b842c5b0b2cf15cb907e76e8bac4843e90830a62a68bbe877a5",
    "zh:bd640d7e8a126d810a34766816b4e17a07c634ffef14b468269c8191683fff27",
    "zh:ddfa43a7b31fb840f04420c82fe0313a44fa5099c3d1f61219e630d6c8440e2d",
    "zh:e50dccaf8cb9922ac25e2f87a85083d5c2cef5323eac4ce7d933012af7a25e88",
    "zh:e72903aeb4830b7b89efcf7336a61c736d9049c4156b6f17cec51663ed6e803d",
    "zh:f4161d62960ec9f9d84cb73437a9b9195831c467cdcc3381e431fa6e2cd92a14",
    "zh:f4699da872dfc9847eb3da49fd6ae4943e92602b617931bb07b91e646d90a279",
  ]
}

provider "registry.opentofu.org/hashicorp/helm" {
  version = "2.14.0"
  hashes = [
    "h1:ibK3MM61pVjBwBcrro56OLTHwUhhNglvGG9CloLvliI=",
    "zh:1c84ca8c274564c46497e89055139c7af64c9e1a8dd4f1cd4c68503ac1322fb8",
    "zh:211a763173934d30c2e49c0cc828b1e34a528b0fdec8bf48d2bb3afadd4f9095",
    "zh:3dca0b703a2f82d3e283a9e9ca6259a3b9897b217201f3cddf430009a1ca00c9",
    "zh:40c5cfd48dcef54e87129e19d31c006c2e3309ee6c09d566139eaf315a59a369",
    "zh:6f23c00ca1e2663e2a208a7491aa6dbe2604f00e0af7e23ef9323206e8f2fc81",
    "zh:77f8cfc4888600e0d12da137bbdb836de160db168dde7af26c2e44cf00cbf057",
    "zh:97b99c945eafa9bafc57c3f628d496356ea30312c3df8dfac499e0f3ff6bf0c9",
    "zh:a01cfc53e50d5f722dc2aabd26097a8e4d966d343ffd471034968c2dc7a8819d",
    "zh:b69c51e921fe8c91e38f4a82118d0b6b0f47f6c71a76f506fde3642ecbf39911",
    "zh:fb8bfc7b8106bef58cc5628c024103f0dd5276d573fe67ac16f343a2b38ecee8",
  ]
}

provider "registry.opentofu.org/hashicorp/kubernetes" {
  version     = "2.13.1"
  constraints = ">= 2.13.0, ~> 2.13.0"
  hashes = [
    "h1:fY2gWOd+w5CKV+B+T9LugeprJxHjjkaW81QZ4Urpzlk=",
    "zh:15d61e1ea6e428437e5947330a8328ed62b61bca01e0f2584e4e80d0aaea93ab",
    "zh:61183fddab8988a892f459abb38d9d6e50b8db0e2b252e398a10270e9a4f3328",
    "zh:67db11bf1596595ada2ae4e8fce13ac3e7ffd242ff8f6a2440bd8086c0d2cd09",
    "zh:6be96982825d821df7b29dc0c40c2f5807561e6af33e858668d1e06e1df3efc3",
    "zh:6e391b9601349ded72135e5f9159161f3cf9042e272611c2b5d863bda2724302",
    "zh:73dd61f4dc6f03f58c643a5dcee8170520f38ec06248186b651ea3d951ed7d6a",
    "zh:92b2f3f0657cfe0ad7cfba72e89246f5d02be53389a56fc205716b549e4848cd",
    "zh:a7ff7812eebdcaf75bb8937acc742d8f7d796d2df93d2c2feb27c5813e548da1",
    "zh:ac2edd95b356ac0fb48569c825cb91dfc0820c688ef12cc657c2305b8f12be94",
    "zh:eabae340b652d63f8f127771e70af854671bf76014f8c53e1f95b09d42729e0d",
  ]
}

provider "registry.opentofu.org/hashicorp/random" {
  version     = "3.3.2"
  constraints = "~> 3.3.2"
  hashes = [
    "h1:3cVQP2pZt7WXSmZVYRj75U8PTP17Tbo+LubWAJJvt6s=",
    "zh:06940c8bc66b49e27c4a7030242df2211be2635bc061b656c9110b521f0d6f71",
    "zh:0aa9d7f1d7971b662485ca2474fc13ab2dba7951ad56f37d08ccf92c2e918bec",
    "zh:0e265e4154792c79865c27c55661f63c3df56e9ab961f47c4014f255e4aa3c33",
    "zh:2d8305ed9ddd1907b81a208170c7599ffb99eacf2639cf40b5c7fe384585ee87",
    "zh:43c7dd999908ead0e98a053c294b7d53546c45d6317e9124df39f6ed31e5fce8",
    "zh:5ddce4cb91ddda675071166d975cc5af2ccb5efabbf327e9e5d21f0b93c9ab6c",
    "zh:777ef4bba1f1875c4bcda9c2207bb00a24758a2a6c9097446c84d7cc20356673",
    "zh:81542311f3d1fac213c9c25d3650de2a0d54cc480ec9c5abc16d88f9802b82b0",
    "zh:9031150598307c66e61c13f7ca7b750ff8c4e373dc912f869d8ca81f9d1b4a2e",
    "zh:9c8caf2248dadd21480bd2705680d76e9939f2b5956b6863789bbe0ec5457892",
  ]
}

provider "registry.opentofu.org/hashicorp/tls" {
  version     = "4.0.5"
  constraints = "~> 4.0.4"
  hashes = [
    "h1:zEH0OgSkeXDqNWzmOUWDczrUwyyujAHvnbW79qdxVMI=",
    "zh:05a7dc3ac92005485714f87541ad6d0d478988b478c5774227a7d39b01660050",
    "zh:547e0def44080456169bf77c21037aa6dc9e7f3e644a8f6a2c5fc3e6c15cf560",
    "zh:6842b03d050ae1a4f1aaed2a2b1ca707eae84ae45ae492e4bb57c3d48c26e1f1",
    "zh:6ced0a9eaaba12377f3a9b08df2fd9b83ae3cb357f859eb6aecf24852f718d9a",
    "zh:766bcdf71a7501da73d4805d05764dcb7c848619fa7c04b3b9bd514e5ce9e4aa",
    "zh:84cc8617ce0b9a3071472863f43152812e5e8544802653f636c866ef96f1ed34",
    "zh:b1939e0d44c89315173b78228c1cf8660a6924604e75ced7b89e45196ce4f45e",
    "zh:ced317916e13326766427790b1d8946c4151c4f3b0efd8f720a3bc24abe065fa",
    "zh:ec9ff3412cf84ba81ca88328b62c17842b803ef406ae19152c13860b356b259c",
    "zh:ff064f0071e98702e542e1ce00c0465b7cd186782fe9ccab8b8830cac0f10dd4",
  ]
}

provider "registry.opentofu.org/scaleway/scaleway" {
  version     = "2.19.0"
  constraints = "~> 2.19.0"
  hashes = [
    "h1:kFQEl0nLEVh8z2ug6TaW8Y9m4i0T2ClgUsTMOMST6Ps=",
    "zh:01bb05980de9c7b394551f92e2723de2a4058367e94884590530d439bae38ddb",
    "zh:0c3bd636af851a54601dc6620282eef016108d5e7cc1225fe12aa2eb5017df9b",
    "zh:1cf3ad0cf99d36cb48988b380d4a2e11e14f8da56347e6873df35aa9b4f5d6ff",
    "zh:3ce5d920e593e7d358d9bd79bdba394a1514b4d83039007f7c282e2f88c88f50",
    "zh:404ec673690d55cdee2bf0fd6778c63d10d64e0fdfc96c9be5d63b2a1f793e28",
    "zh:467f1b6f6cf0b47ff8f2b1236f432ac9c9432b8469dfc3fd5fe5eb182bde9040",
    "zh:4f5edbc19a8e90ef76347901ddec1044ba46115ed0edf5eddb20710252b2aba6",
    "zh:517fe6eb15d4308c3e446b65cbe590b3c67b2370fb19f224d489786f8ff362ea",
    "zh:58ba43c12ba067c7af143a971d207001bd1e22fd561d1a2312b509202a6c2e13",
    "zh:9f33a12eabb12262c40c1f568bef85bc756b9e4f268a1f032f29df2875b12598",
    "zh:cd1c2878d30fceff09a1a7844b27b42678221afab4ce4b9e510614b7f08c54f7",
    "zh:cedd72dcfe96bad66e4018377d67aed1d676852574b4c0452bb3beae6453b82f",
    "zh:d3c366a9d80096273b36092a5d5272ab8ea672517e64619d7d8b7011b391f5b6",
    "zh:fb8bd20f1d31c36a6864fd4e42ddaca83eac43b2712167059b542950d365ac38",
  ]
}
